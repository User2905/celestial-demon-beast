-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2023 at 08:41 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dealer_mobil1`
--

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `kode_pegawai` varchar(10) NOT NULL,
  `nama_pegawai` varchar(25) NOT NULL,
  `password` varchar(10) NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`kode_pegawai`, `nama_pegawai`, `password`, `email`) VALUES
('2022310016', 'sabil', 'SABIL', 'salsabil123@gmail.com'),
('2022340011', 'fadhil arvian', 'arvia', 'arvianchinaindo12@gmail.com'),
('K002', 'Kevin Mubarak', 'UKK2905', 'Kevin@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `nama_pembeli` varchar(20) NOT NULL,
  `merk_mobil` varchar(20) NOT NULL,
  `jenis_mobil` varchar(10) NOT NULL,
  `harga` int(11) NOT NULL,
  `pembayaran` enum('TUNAI','KREDIT','','') NOT NULL,
  `diskon` int(11) NOT NULL,
  `total_bayar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`nama_pembeli`, `merk_mobil`, `jenis_mobil`, `harga`, `pembayaran`, `diskon`, `total_bayar`) VALUES
('Kevin', 'AVANZA', 'SEDAN', 13500000, 'TUNAI', 2700000, 10800000),
('Reyhan', 'SIGRA', 'SEDAN', 33000000, 'TUNAI', 6600000, 26400000),
('Kevin Mubarak', 'HONDA CIVIC', 'SEDAN', 33000000, 'TUNAI', 9900000, 23100000),
('Kevin Mubarak2', 'KIJANG INOVA', 'SEDAN', 12500000, 'KREDIT', 625000, 11875000),
('SABIL', 'HONDA CIVIC', 'SEDAN', 33000000, 'TUNAI', 9900000, 23100000),
('fadhil ', 'HONDA CIVIC', 'SEDAN', 33000000, 'TUNAI', 9900000, 23100000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`kode_pegawai`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
